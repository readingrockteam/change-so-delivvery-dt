﻿
Imports System.Reflection

Public Class Form1

    Private _m2mCatalog as String
    private _psCatalog as String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim sqlconn As New SqlClient.SqlConnection
            sqlconn.ConnectionString = My.Settings.m2mConn
            sqlconn.Open()
            sqlconn.Close()
            sqlconn.Dispose()
        Catch ex As Exception
            ' if we cannot connect, then display the SqlConnWiz
            Dim results As DialogResult
            SQLConnWizard.Text = "Configure connection to M2M"
            results = SQLConnWizard.ShowDialog(Me)
            If results = Windows.Forms.DialogResult.OK Then
                My.Settings.m2mConnection = SQLConnWizard.sConnection
                My.Settings.Save()
            Else
                MsgBox("You did not create a successful connection to M2M.", MsgBoxStyle.Exclamation, "Shutting down..")
                End
            End If
        End Try

        Try
            Dim sqlconn As New SqlClient.SqlConnection
            sqlconn.ConnectionString = My.Settings.Packing_SolutionConn
            sqlconn.Open()
            sqlconn.Close()
            sqlconn.Dispose()
        Catch ex As Exception
            ' if we cannot connect, then display the SqlConnWiz
            Dim results As DialogResult
            SQLConnWizard.Text = "Configure connection to Packing Solution"
            results = SQLConnWizard.ShowDialog(Me)
            If results = Windows.Forms.DialogResult.OK Then
                My.Settings.psConnection = SQLConnWizard.sConnection
                My.Settings.Save()
            Else
                MsgBox("You did not create a successful connection to Packing Solution.", MsgBoxStyle.Exclamation, "Shutting down..")
                End
            End If
        End Try
        UpdateTitle()
    End Sub

    Private Sub UpdateTitle()

        Dim sConnArr As String() = My.Settings.M2MConn.Split(";")
        Dim pConnArr as String() = My.Settings.Packing_SolutionConn.Split(";")
        _m2mCatalog = GetSource(sConnArr, _m2mCatalog)        _psCatalog = GetSource(pConnArr, _psCatalog)
        Text = "Change SO Delievery Date v." + Assembly.GetExecutingAssembly().GetName().Version.ToString() + " M2M: " + _m2mCatalog + " PS: " + _psCatalog
        lblDB.Text = "M2M: " + _m2mCatalog + "     PS: " + _psCatalog
    End Sub

    Private Function GetSource(sConnArr As String(), dSource As String) As String     For Each el As String In sConnArr        Dim aEl As String() = el.Split("=")        If el.Contains("Catalog") Then            dSource = aEl(1)            exit for        End If    Next    Return dSourceEnd Function

    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Dim sqlConnection1 As New System.Data.SqlClient.SqlConnection(My.Settings.sconnM2M)
        Dim bSuccess As Boolean = False
        Dim sErrorStr As String = ""
        Dim cmd As New System.Data.SqlClient.SqlCommand
        cmd.CommandType = System.Data.CommandType.Text
        cmd.CommandText = "update sorels set fdelivery = '" & Mid(dtp.Value.ToString, 1, InStr(dtp.Value.ToString, " ") - 1) & "', fduedate = '" & Mid(dtp.Value.ToString, 1, InStr(dtp.Value.ToString, " ") - 1) & "' from sorels inner join soitem on soitem.fsono = sorels.fsono and soitem.finumber = sorels.finumber where soitem.fsono = '" & FsonoTextBox.Text & "'"
        cmd.Connection = sqlConnection1

        sqlConnection1.Open()
        If chkChangeDate.Checked Then
            Try
                cmd.ExecuteNonQuery() ' update sorels
                bSuccess = True
            Catch ex As Exception
                bSuccess = False
                sErrorStr = "SORELS " & ex.Message.ToString
            End Try
            cmd.CommandText = "update soitem set fduedate = '" & Mid(dtp.Value.ToString, 1, InStr(dtp.Value.ToString, " ") - 1) & "' from sorels inner join soitem on soitem.fsono = sorels.fsono and soitem.finumber = sorels.finumber where soitem.fsono = '" & FsonoTextBox.Text & "'"
            Try
                cmd.ExecuteNonQuery() ' update soitem
                bSuccess = True
            Catch ex As Exception
                bSuccess = False
                sErrorStr &= vbCrLf & "SOITEM " & ex.Message.ToString
            End Try
        End If
        cmd.CommandText = "update soitem_ext set FEXP = " & IIf(chkEXP.Checked, 1, 0) & ", FXFACTOR = " & IIf(chkXF.Checked, 1, 0) & ", FEARLY = " & IIf(chkEarly.Checked, 1, 0) & " from soitem_ext inner join soitem on soitem_ext.fkey_id = soitem.identity_column and soitem.fsono = '" & FsonoTextBox.Text & "'"
        Try
            cmd.ExecuteNonQuery() ' update soitem_ext
            bSuccess = True
        Catch ex As Exception
            bSuccess = False
            sErrorStr &= vbCrLf & "SOITEM_EXT " & ex.Message.ToString
        End Try
        sqlConnection1.Close()
        If bSuccess Then
            MsgBox("Successful Update", MsgBoxStyle.Information, "Did it")
        Else
            MsgBox(sErrorStr, MsgBoxStyle.Exclamation, "Did not work")
        End If

    End Sub

    Private Sub M2MToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles M2MToolStripMenuItem.Click
        Dim results As DialogResult
        SQLConnWizard.Text = "Configure connection to M2M"
        results = SQLConnWizard.ShowDialog(Me)
        If results = Windows.Forms.DialogResult.OK Then
            My.Settings.m2mConnection = SQLConnWizard.sConnection
            My.Settings.Save()
            UpdateTitle()
        Else
            MsgBox("You did not create a successful connection to M2M.", MsgBoxStyle.Exclamation, "Shutting down..")
            End
        End If
    End Sub

    Private Sub PackingSolutionToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackingSolutionToolStripMenuItem.Click
        Dim results As DialogResult
        SQLConnWizard.Text = "Configure connection to Packing Solution"
        results = SQLConnWizard.ShowDialog(Me)
        If results = Windows.Forms.DialogResult.OK Then
            My.Settings.psConnection = SQLConnWizard.sConnection
            My.Settings.Save()
            UpdateTitle()
        Else
            MsgBox("You did not create a successful connection to Packing Solution.", MsgBoxStyle.Exclamation, "Shutting down..")
            End
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        End
    End Sub

    Private Sub AboutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AboutToolStripMenuItem.Click
        Dim frm as New AboutBox1
        frm.dbInfo = vbcrlf + " M2M: " + _m2mCatalog + vbCrLf + " PS: " + _psCatalog
        frm.ShowDialog(Me)
    End Sub
End Class
