﻿Imports System.Windows.Forms
Imports System.Data.SqlClient
Public Class SQLConnWizard
    Public sConnection As String = ""
    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        sConnection = ""
        Me.Close()
    End Sub

    Private Sub Dialog1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        OK_Button.Enabled = False
        txtUser.Text = System.Environment.UserName
        Dim sConnArr As String() = sConnection.Split(";")
        For Each el As String In sConnArr
            Dim aEl As String() = el.Split("=")
            If el.Contains("Data Source") Then
                txtServer.Text = aEl(1)
            End If
            If el.Contains("Catalog") Then
                txtDataBase.Text = aEl(1)
            End If
            If el.Contains("Integrated") Then
                cbSecurity.SelectedValue = aEl(0)
            End If
        Next

    End Sub
    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click
        Dim sqlConn As New SqlConnection
        Dim sTemp As String = ""
        sTemp = "Data Source=" + txtServer.Text.Trim + ";Initial Catalog=" + txtDataBase.Text.Trim
        Select Case cbSecurity.SelectedItem.ToString.Trim
            Case "Persist"
                sTemp &= ";Persist Security Info=True"
            Case "Integrated"
                sTemp &= ";Integrated Security=True"
        End Select
        If txtUser.Text.Trim.Length > 0 Then
            sTemp &= ";User=" + txtUser.Text.Trim
        End If
        If txtPassword.Text.Trim.Length > 0 Then
            sTemp &= ";Password=" + txtPassword.Text.Trim
        End If

        sqlConn.ConnectionString = sTemp
        Try
            sqlConn.Open()
            sqlConn.Close()
            sConnection = sTemp 'sqlConn.ConnectionString.ToString()
            MsgBox("Connection Successful")
            OK_Button.Enabled = True
        Catch ex As SqlClient.SqlException
            MsgBox(ex.Message.ToString(), MsgBoxStyle.Exclamation, "Connection Failed...")
        End Try
    End Sub

    Private Sub txtConfirm_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtUser.Text.Trim <> txtConfirm.Text.Trim Then
            txtConfirm.Clear()
            txtConfirm.Focus()
        End If
    End Sub

End Class
