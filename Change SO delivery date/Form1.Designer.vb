﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim FsonoLabel As System.Windows.Forms.Label
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.cmdUpdate = New System.Windows.Forms.Button()
        Me.dtp = New System.Windows.Forms.DateTimePicker()
        Me.FsonoTextBox = New System.Windows.Forms.TextBox()
        Me.chkEXP = New System.Windows.Forms.CheckBox()
        Me.chkXF = New System.Windows.Forms.CheckBox()
        Me.chkEarly = New System.Windows.Forms.CheckBox()
        Me.chkChangeDate = New System.Windows.Forms.CheckBox()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AdminToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReconfigDBToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.M2MToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PackingSolutionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblDB = New System.Windows.Forms.Label()
        FsonoLabel = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout
        Me.SuspendLayout
        '
        'FsonoLabel
        '
        FsonoLabel.AutoSize = true
        FsonoLabel.Location = New System.Drawing.Point(240, 86)
        FsonoLabel.Name = "FsonoLabel"
        FsonoLabel.Size = New System.Drawing.Size(25, 13)
        FsonoLabel.TabIndex = 15
        FsonoLabel.Text = "SO:"
        '
        'cmdUpdate
        '
        Me.cmdUpdate.Location = New System.Drawing.Point(296, 109)
        Me.cmdUpdate.Name = "cmdUpdate"
        Me.cmdUpdate.Size = New System.Drawing.Size(75, 23)
        Me.cmdUpdate.TabIndex = 0
        Me.cmdUpdate.Text = "Update"
        Me.cmdUpdate.UseVisualStyleBackColor = true
        '
        'dtp
        '
        Me.dtp.Location = New System.Drawing.Point(13, 83)
        Me.dtp.Name = "dtp"
        Me.dtp.Size = New System.Drawing.Size(200, 20)
        Me.dtp.TabIndex = 1
        '
        'FsonoTextBox
        '
        Me.FsonoTextBox.Location = New System.Drawing.Point(271, 83)
        Me.FsonoTextBox.Name = "FsonoTextBox"
        Me.FsonoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.FsonoTextBox.TabIndex = 16
        '
        'chkEXP
        '
        Me.chkEXP.AutoSize = true
        Me.chkEXP.Location = New System.Drawing.Point(13, 110)
        Me.chkEXP.Name = "chkEXP"
        Me.chkEXP.Size = New System.Drawing.Size(47, 17)
        Me.chkEXP.TabIndex = 17
        Me.chkEXP.Text = "EXP"
        Me.chkEXP.UseVisualStyleBackColor = true
        '
        'chkXF
        '
        Me.chkXF.AutoSize = true
        Me.chkXF.Location = New System.Drawing.Point(66, 110)
        Me.chkXF.Name = "chkXF"
        Me.chkXF.Size = New System.Drawing.Size(39, 17)
        Me.chkXF.TabIndex = 17
        Me.chkXF.Text = "XF"
        Me.chkXF.UseVisualStyleBackColor = true
        '
        'chkEarly
        '
        Me.chkEarly.AutoSize = true
        Me.chkEarly.Location = New System.Drawing.Point(111, 110)
        Me.chkEarly.Name = "chkEarly"
        Me.chkEarly.Size = New System.Drawing.Size(73, 17)
        Me.chkEarly.TabIndex = 17
        Me.chkEarly.Text = "Early Ship"
        Me.chkEarly.UseVisualStyleBackColor = true
        '
        'chkChangeDate
        '
        Me.chkChangeDate.AutoSize = true
        Me.chkChangeDate.Checked = true
        Me.chkChangeDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkChangeDate.Location = New System.Drawing.Point(13, 52)
        Me.chkChangeDate.Name = "chkChangeDate"
        Me.chkChangeDate.Size = New System.Drawing.Size(111, 17)
        Me.chkChangeDate.TabIndex = 17
        Me.chkChangeDate.Text = "Change the date?"
        Me.chkChangeDate.UseVisualStyleBackColor = true
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AdminToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(436, 24)
        Me.MenuStrip1.TabIndex = 18
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'AdminToolStripMenuItem
        '
        Me.AdminToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReconfigDBToolStripMenuItem})
        Me.AdminToolStripMenuItem.Name = "AdminToolStripMenuItem"
        Me.AdminToolStripMenuItem.Size = New System.Drawing.Size(55, 20)
        Me.AdminToolStripMenuItem.Text = "Admin"
        '
        'ReconfigDBToolStripMenuItem
        '
        Me.ReconfigDBToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.M2MToolStripMenuItem, Me.PackingSolutionToolStripMenuItem})
        Me.ReconfigDBToolStripMenuItem.Name = "ReconfigDBToolStripMenuItem"
        Me.ReconfigDBToolStripMenuItem.Size = New System.Drawing.Size(139, 22)
        Me.ReconfigDBToolStripMenuItem.Text = "Reconfig DB"
        '
        'M2MToolStripMenuItem
        '
        Me.M2MToolStripMenuItem.Name = "M2MToolStripMenuItem"
        Me.M2MToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.M2MToolStripMenuItem.Text = "M2M"
        '
        'PackingSolutionToolStripMenuItem
        '
        Me.PackingSolutionToolStripMenuItem.Name = "PackingSolutionToolStripMenuItem"
        Me.PackingSolutionToolStripMenuItem.Size = New System.Drawing.Size(163, 22)
        Me.PackingSolutionToolStripMenuItem.Text = "Packing Solution"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.AboutToolStripMenuItem.Text = "About"
        '
        'lblDB
        '
        Me.lblDB.Font = New System.Drawing.Font("Microsoft Sans Serif", 9!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0,Byte))
        Me.lblDB.Location = New System.Drawing.Point(10, 24)
        Me.lblDB.Name = "lblDB"
        Me.lblDB.Size = New System.Drawing.Size(400, 15)
        Me.lblDB.TabIndex = 19
        Me.lblDB.Text = "No Connection No connectoin no connection"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(436, 140)
        Me.Controls.Add(Me.lblDB)
        Me.Controls.Add(Me.chkEarly)
        Me.Controls.Add(Me.chkXF)
        Me.Controls.Add(Me.chkChangeDate)
        Me.Controls.Add(Me.chkEXP)
        Me.Controls.Add(FsonoLabel)
        Me.Controls.Add(Me.FsonoTextBox)
        Me.Controls.Add(Me.dtp)
        Me.Controls.Add(Me.cmdUpdate)
        Me.Controls.Add(Me.MenuStrip1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = false
        Me.Name = "Form1"
        Me.Text = "Change Delivery Date on Sales Order"
        Me.MenuStrip1.ResumeLayout(false)
        Me.MenuStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents cmdUpdate As System.Windows.Forms.Button
    Friend WithEvents dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents FsonoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents chkEXP As System.Windows.Forms.CheckBox
    Friend WithEvents chkXF As System.Windows.Forms.CheckBox
    Friend WithEvents chkEarly As System.Windows.Forms.CheckBox
    Friend WithEvents chkChangeDate As System.Windows.Forms.CheckBox
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AdminToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReconfigDBToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents M2MToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PackingSolutionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents lblDB As Label
End Class
